### Bienvenue

Le hackathon à l'assemblée nationale #Datafin vous remercie pour votre participation à ce questionnaire !

Nous vous proposons d'**imaginer que vous êtes un habitant qui va emménager dans un nouveau quartier de 80 hectares**. Les élus locaux sollicitent votre avis sur le réaménagement de ce quartier. Votre avis concernera l'aménagement des structures proche de votre futur logement et accessibles à pied. Ce quartier est situé en zone urbaine au sein d'une ville qui dispose déjà de structures conséquentes (piscine, salle de spectacle, patinoire...) situées hors du quartier en question.

Ce questionnaire devrait vous prendre **15 minutes au total**. Bonne séance !