<div class="bg-info d-flex flex-row mb-4 text-white">
  <img alt="Blason d'Amiens" class="bg-white mb-1 ml-1 mr-1 mt-1" src="../logo.png" style="height: 128px; width: auto;">
  <div class="align-self-center flex-grow-1 text-center">
    <h1>Ville d'Amiens</h1>
    <h2>Donnez votre avis sur le budget municipal</h2>
  </div>
</div>
