# Police municipale

_Leur métier : assurer la sécurité de proximité._

**Les hypothèses financières** pour les estimations de coûts de chaque option liée à la police municipale sont les suivantes :
- Un poste de police municipale a une superficie moyenne de _500 m2_ pour un coût d'investissement moyen de _1 069 € / m2_.
- Le poste de police acceuillera _8 agents_ chargés d'assurer la sécurité de proximité. 
- Le coût de fonctionnement de l'ordre de _35 000 € par agent à plein temps_.

**Aujourd’hui**, la police municipale comporte 200 agents répartis sur 6 sites. Cela représente 0,9% du budget de fonctionnement de la mairie. Le nombre de vols et de dégradation est de 44 pour 1000 habitants par an.

**Par comparaison**, la moyenne par rapport aux villes de taille similaire est de 0,7% du budget de fonctionnement d'une mairie. En France, le nombre moyen de vols et de dégradation est de 30 pour 1000 habitants par an.

**Les tendances** de ces dernières années : sur ce poste, le budget municipal a augmenté de 5% par an ces 3 dernières années et le nombre de vols et de dégradation s'est réduit de 3 pour 1000 habitants par an.

![Policiers municipaux en train de patrouiller dans un marché](police-municipale.jpg)